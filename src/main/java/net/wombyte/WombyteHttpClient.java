package net.wombyte;


import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class WombyteHttpClient {
    private final String token;
    private final String secret;

    private final URL base;
    private final int timeout;

    public WombyteHttpClient(String host,
                             String token,
                             String secret,
                             boolean secure,
                             int timeout) throws MalformedURLException {

        this.token = token;
        this.secret = secret;
        this.timeout = timeout;

        this.base = new URL((secure ? "https://" : "http://") + host + "/");
    }

    public boolean sendFile(File file) throws IOException {
        HttpURLConnection connection = getHttpURLConnection();

        try (OutputStream out = connection.getOutputStream();
             RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
             FileChannel channel = randomAccessFile.getChannel();
             FileLock lock = channel.tryLock()) {

            int bufferSize = 4096;
            if (bufferSize > channel.size()) {
                bufferSize = (int) channel.size();
            }

            ByteBuffer buff = ByteBuffer.allocate(bufferSize);

            while (channel.read(buff) > 0) {
                out.write(buff.array(), 0, buff.position());

                buff.clear();
            }

            out.flush();

            return connection.getResponseCode() < 500;
        } finally {
            connection.disconnect();
        }
    }

    private HttpURLConnection getHttpURLConnection() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) base.openConnection();

        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Wombyte-Access-Token", token);
        connection.setRequestProperty("Wombyte-Access-Key", secret);

        return connection;
    }


}
