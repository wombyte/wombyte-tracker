package net.wombyte;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface Event {

    Event addStrings(Map<String,String> attrs);

    Event addString (String key, String value);

    Event addNumerals(Map<String, Double> attrs);

    Event addNumeral(String key, double value);

    CompletableFuture send();
}
