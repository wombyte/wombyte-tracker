package net.wombyte.tracker;

import net.wombyte.EventTracker;

public interface TrackerBuilder {
    EventTracker build(String app);
}
