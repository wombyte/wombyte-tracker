package net.wombyte.tracker;

import net.wombyte.Event;
import net.wombyte.EventTracker;
import net.wombyte.buffer.WombyteBuffer;
import net.wombyte.event.WombyteEvent;
import net.wombyte.event.WombyteEventContext;

import java.util.concurrent.CompletableFuture;

public class WombyteTracker implements EventTracker {
    private final WombyteBuffer buffer;
    private final String app;

    WombyteTracker(WombyteBuffer buffer, String app) {
        this.buffer = buffer;
        this.app = app;
    }

    @Override
    public Event track(String event) {
        return new WombyteEvent(new WombyteEventContext(event, app), buffer);
    }

    @Override
    public CompletableFuture<Void> flush() {
        return CompletableFuture.runAsync(buffer::flushLines);
    }

    @Override
    public void close() {
        buffer.close();
    }
}
