package net.wombyte.tracker;

public interface RemoteSetter {
    TrackerBuilder setRemote(String host, String token, String secret);
    TrackerBuilder setRemote(String host, String token, String secret, boolean secure, int timeout);
}
