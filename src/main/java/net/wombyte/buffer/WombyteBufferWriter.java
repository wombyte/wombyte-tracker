package net.wombyte.buffer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Closeable;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class WombyteBufferWriter implements Closeable {
    private final static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
    }

    private FileChannel channel;
    private final String file;

    WombyteBufferWriter(String file) {
        this.file = file;
    }

    void write(Object[] records) {
        try {
            FileChannel lazyChanel = getWriter();

            lazyChanel.write(ByteBuffer.wrap(mapper.writeValueAsBytes(records)));
            lazyChanel.write(ByteBuffer.wrap("\n".getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (channel != null) {
                channel.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private FileChannel getWriter() throws IOException {
        if (channel == null) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            channel = randomAccessFile.getChannel();
            channel.tryLock();
        }

        return channel;
    }
}
