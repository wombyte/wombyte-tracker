package net.wombyte.buffer;

import net.wombyte.WombyteHttpClient;
import net.wombyte.event.WombyteEventContext;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class WombyteBuffer implements Closeable {

    private final ReadWriteLock writeLock = new ReentrantReadWriteLock();
    private final ReadWriteLock sendLock = new ReentrantReadWriteLock();

    private final File directory;
    private final WombyteHttpClient client;

    private WombyteBufferWriter writer;

    public WombyteBuffer(File directory, WombyteHttpClient client) {
        this.directory = directory;
        this.client = client;

        this.writer = createWriter();
    }

    private WombyteBufferWriter createWriter() {
        return new WombyteBufferWriter(directory.toString() + "/" + UUID.randomUUID());
    }

    private File[] rotate() {
        File[] files = directory.listFiles();

        writeLock.writeLock().lock();

        try {
            writer.close();
            writer = createWriter();
        } finally {
            writeLock.writeLock().unlock();
        }

        return files;
    }

    public CompletableFuture<Boolean> append(WombyteEventContext context, Map<String, Object> attributes) {
        Object[] record = new Object[]
        {
            context.timestamp,
            context.id,
            context.event,
            context.app,

            attributes
        };

        return CompletableFuture.supplyAsync(() -> {
            writeLock.writeLock().lock();

            try {
                writer.write(record);
            } finally {
                writeLock.writeLock().unlock();
            }

            return true;
        });

    }

    @Override
    public void close() {
        writer.close();
    }

    public void flushLines() {
        sendLock.writeLock().lock();

        try {
            for (File file : rotate()) {
                if (client.sendFile(file)) {
                    Files.delete(file.toPath());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            sendLock.writeLock().unlock();
        }
    }


}
