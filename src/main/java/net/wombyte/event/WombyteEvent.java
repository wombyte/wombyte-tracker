package net.wombyte.event;

import net.wombyte.buffer.WombyteBuffer;
import net.wombyte.Event;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static net.wombyte.Wombyte.*;

public class WombyteEvent implements Event {

    private final Map<String, Object> attributes = new HashMap<>();
    private final WombyteEventContext context;
    private final WombyteBuffer buffer;

    public WombyteEvent(WombyteEventContext context, WombyteBuffer buffer) {
        this.context = context;
        this.buffer = buffer;
    }

    @Override
    public Event addStrings(Map<String, String> attrs) {
        for (Map.Entry<String, String> entry : attrs.entrySet()) {
            attributes.put(toField(entry.getKey()), toValue(entry.getValue()));
        }

        return this;
    }

    @Override
    public Event addString(String key, String value) {
        attributes.put(toField(key), toValue(value));

        return this;
    }

    @Override
    public Event addNumerals(Map<String, Double> attrs) {
        for (Map.Entry<String, Double> entry : attrs.entrySet()) {
            attributes.put(toField(entry.getKey()), entry.getValue());
        }

        return this;
    }

    @Override
    public Event addNumeral(String key, double value) {
        attributes.put(toField(key), value);

        return this;
    }

    @Override
    public CompletableFuture<Boolean> send() {
        int numerals = 0;
        int strings = 0;

        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            if (entry.getValue() instanceof Double) {
                numerals += 1;
            } else if (entry.getValue() instanceof String) {
                strings += 1;
            }
        }

        return numerals <= 30 && strings <= 30
                ? buffer.append(context, attributes)
                : CompletableFuture.completedFuture(false);
    }
}
